# Node & npm
---
## Node

#### What is node? 
Node is a JavaScript runtime built off of the Chrome V8 engine, an Open Source JavaScript engine. All this means is that you can run JavaScript with node outside of the browser with full support for system I/O and other essential utilities for work locally or on a server. It operates well across Linux, MacOS, and Windows.

#### Installing Node
Navigate to their [web site](https://nodejs.org/en/) and download the "Recommended for Most Users" option and then install it. That is it! To confirm it installed correctly run the following command in the command line: __node -v__. It should simply print out the version of node you installed. 

#### Using Node
...to come

## npm
#### npm? 
NPM is the package manager for node where you publish packages (libraries) that other people can download and use in their projects. NPM gives a very convenient set of tools and commands to manage this with a robust CLI.

#### Installing npm
NPM is installed and bundled with the Node installation above, so installing npm is no more than installing node! To check and confirm npm installed correctly run the following in the command line: __npm --version__. It should print out the version of npm in the command line. NOTE: the npm version is separate from the Node version

#### Using npm
... to come